package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/beamexp"
	ctl "bitbucket.org/uwaploe/iccdctl"
	"bitbucket.org/uwaploe/imagesvc"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"bitbucket.org/uwaploe/viron"
	"github.com/briandowns/spinner"
	prompt "github.com/c-bata/go-prompt"
	"github.com/pkg/errors"
	"google.golang.org/grpc/metadata"
)

type Commander interface {
	Help() string
	Synopsis() string
	Validate(args []string) error
	Completion() []prompt.Suggest
	Run(ctx context.Context, args []string) (string, error)
}

func sendRecv(bus *ctl.Bus, addr int, m ctl.Message) (ctl.Message, error) {
	var resp ctl.Message
	err := bus.Send(m, addr)
	if err != nil {
		return resp, errors.Wrap(err, "send message")
	}
	return bus.Recv()
}

type pingCommand struct {
	bus    *ctl.Bus
	target int
}

func (c *pingCommand) Help() string {
	return "usage: ping"
}

func (c *pingCommand) Synopsis() string {
	return "check that ICCD is responding to commands"
}

func (c *pingCommand) Validate(args []string) error {
	return nil
}

func (c *pingCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *pingCommand) Run(ctx context.Context, args []string) (string, error) {
	msg := ctl.NewMessage(ctl.CmdPing, 0)
	resp, err := sendRecv(c.bus, c.target, msg)
	if err != nil {
		return "", err
	}
	if resp.Cmd != ctl.CmdPong {
		return "", fmt.Errorf("Unexpected response: %#x", resp.Cmd)
	}
	return "pong", nil
}

type resetCommand struct {
	bus    *ctl.Bus
	target int
}

func (c *resetCommand) Help() string {
	return "usage: reset"
}

func (c *resetCommand) Synopsis() string {
	return "perform a software reset of the ICCD"
}

func (c *resetCommand) Validate(args []string) error {
	return nil
}

func (c *resetCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *resetCommand) Run(ctx context.Context, args []string) (string, error) {
	msg := ctl.NewMessage(ctl.CmdReset, 0)
	err := c.bus.Send(msg, c.target)
	return "", err
}

type gateCommand struct {
	dev *ctl.Intensifier
}

func (c *gateCommand) Help() string {
	return "usage: gate [internal|external [width]]"
}

func (c *gateCommand) Synopsis() string {
	return "get/set intensifier gate source and width"
}

func (c *gateCommand) Validate(args []string) error {
	if len(args) == 0 {
		return nil
	}

	switch args[0] {
	case "internal":
		if len(args) < 2 {
			return fmt.Errorf("Missing pulse width")
		}
	case "external":
		return nil
	default:
		return fmt.Errorf("Invalid pulse source: %q", args[0])
	}

	_, err := time.ParseDuration(args[1])
	return err
}

func (c *gateCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "external", Description: "use external gate pulse"},
		{Text: "internal", Description: "use internal pulse, width must be specified"},
	}
}

func (c *gateCommand) getGate() (string, error) {
	g, err := c.dev.Gate()
	if err != nil {
		return "", err
	}
	return g.String(), nil
}

func (c *gateCommand) Run(ctx context.Context, args []string) (string, error) {
	if len(args) > 0 {
		var g ctl.Gate
		switch args[0] {
		case "external":
			g.Type = ctl.ExternalGate
		case "internal":
			g.Type = ctl.InternalGate
			g.PulseWidth, _ = time.ParseDuration(args[1])
		}

		err := c.dev.SetGate(g)
		if err != nil {
			return "", err
		}
	}

	return c.getGate()
}

type gainCommand struct {
	dev *ctl.Intensifier
}

func (c *gainCommand) Help() string {
	return "usage: gain [fraction]"
}

func (c *gainCommand) Synopsis() string {
	return "get/set intensifier gain as a fraction of max value"
}

func (c *gainCommand) Validate(args []string) error {
	if len(args) == 0 {
		return nil
	}

	scale, err := strconv.ParseFloat(args[0], 32)
	if err != nil {
		return err
	}
	if scale > 1 || scale < 0 {
		return fmt.Errorf("Value out of range")
	}

	return nil
}

func (c *gainCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *gainCommand) getGain() (string, error) {
	g, err := c.dev.Gain()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%.3f", g), nil
}

func (c *gainCommand) Run(ctx context.Context, args []string) (string, error) {
	if len(args) > 0 {
		g, _ := strconv.ParseFloat(args[0], 32)
		err := c.dev.SetGain(float32(g))
		if err != nil {
			return "", err
		}
	}

	return c.getGain()
}

type syncCommand struct {
	dev *ctl.Intensifier
}

func (c *syncCommand) Help() string {
	return "usage: sync [internal|external]"
}

func (c *syncCommand) Synopsis() string {
	return "get/set sync (trigger) source"
}

func (c *syncCommand) Validate(args []string) error {
	if len(args) == 0 {
		return nil
	}

	switch args[0] {
	case "internal":
		return nil
	case "external":
		return nil
	default:
		return fmt.Errorf("Invalid sync source: %q", args[0])
	}

	return nil
}

func (c *syncCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "external", Description: "use external sync source (max 20khz)"},
		{Text: "internal", Description: "use internal sync source (camera Vsync)"},
	}
}

func (c *syncCommand) getSync() (string, error) {
	s, err := c.dev.Sync()
	if err != nil {
		return "", err
	}
	switch s {
	case ctl.ExternalSync:
		return "external", nil
	case ctl.InternalSync:
		return "internal", nil
	}

	return "unknown", nil
}

func (c *syncCommand) Run(ctx context.Context, args []string) (string, error) {
	if len(args) > 0 {
		var s ctl.SyncSource
		switch args[0] {
		case "external":
			s = ctl.ExternalSync
		case "internal":
			s = ctl.InternalSync
		}

		err := c.dev.SetSync(s)
		if err != nil {
			return "", err
		}
	}

	return c.getSync()
}

type traceCommand struct {
}

func (c *traceCommand) Help() string {
	return "usage: trace on|off"
}

func (c *traceCommand) Synopsis() string {
	return "enable or disable message tracing"
}

func (c *traceCommand) Validate(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Missing argument")
	}
	return nil
}

func (c *traceCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "on", Description: "enable message tracing"},
		{Text: "off", Description: "disable message tracing"},
	}
}

func tracer(fmtstr string, args ...interface{}) {
	fmt.Printf(fmtstr, args...)
}

func (c *traceCommand) Run(ctx context.Context, args []string) (string, error) {
	switch args[0] {
	case "on":
		ctl.TraceFunc = tracer
	case "off":
		ctl.TraceFunc = nil
	}
	return "", nil
}

var FpgaEnable tb.Message = tb.Message{Cmd: "enable"}
var FpgaDisable tb.Message = tb.Message{Cmd: "disable"}
var FpgaLoad tb.Message = tb.Message{Cmd: "load"}

func sendMessages(sock string, msgs ...tb.Message) error {
	conn, err := net.Dial("unix", sock)
	if err != nil {
		return fmt.Errorf("Server connect failed: %v", err)
	}
	defer conn.Close()

	enc := json.NewEncoder(conn)
	for _, msg := range msgs {
		err := enc.Encode(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

type imageCommand struct {
	client      *imagesvc.Client
	opts        []imagesvc.AcqOption
	timing      tb.Timing
	useHardware bool
}

func (c *imageCommand) setTiming() {
	c.timing = tb.Timing{
		Dfreq:       tb.Duration(time.Second),
		EDon:        tb.Duration(time.Microsecond * 60),
		QSdelay:     tb.Duration(time.Microsecond * 300),
		QSETdelay:   tb.Duration(time.Nanosecond * 199800),
		ETon:        tb.Duration(time.Microsecond * 30),
		QSETEGdelay: tb.Duration(time.Nanosecond * 199800),
		EGon:        tb.Duration(time.Microsecond * 30),
	}
}

func (c *imageCommand) Help() string {
	return "usage: image [--exp=EXPOSURE][--gain=dB][--trig=TYPE]"
}

func (c *imageCommand) Synopsis() string {
	return "acquire a single image"
}

func (c *imageCommand) Validate(args []string) error {
	fs := flag.NewFlagSet("image", flag.ContinueOnError)
	exposure := fs.Duration("exp", 0, "set camera exposure")
	timeout := fs.Duration("timeout", 0, "set image acquisition timeout")
	gain := fs.Float64("gain", -1, "set camera gain in dB")
	trig := fs.String("trig", "", "set trigger type")
	err := fs.Parse(args)
	if err != nil {
		return err
	}

	c.opts = make([]imagesvc.AcqOption, 0)
	c.setTiming()

	if *exposure != time.Duration(0) {
		c.opts = append(c.opts, imagesvc.Exposure(*exposure))
	}

	switch strings.ToLower(*trig) {
	case "hardware":
		c.opts = append(c.opts, imagesvc.Trigger(imagesvc.HardwareTrigger))
		c.useHardware = true
	case "software":
		c.opts = append(c.opts, imagesvc.Trigger(imagesvc.SoftwareTrigger))
		c.useHardware = false
	case "":
	default:
		return fmt.Errorf("Invalid trigger type: %q", *trig)
	}

	if *gain >= 0 {
		c.opts = append(c.opts, imagesvc.Gain(float32(*gain)))
	}
	if *timeout != time.Duration(0) {
		c.opts = append(c.opts, imagesvc.Timeout(*timeout))
	}

	return nil
}

func (c *imageCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "--exp", Description: "set camera exposure value (duration)"},
		{Text: "--gain", Description: "set camera gain (dB)"},
		{Text: "--trig", Description: "set trigger type (hardware or software)"},
		{Text: "--timeout", Description: "set image acquisition timeout"},
	}
}

func genFilename(t time.Time) string {
	s := t.Format("20060102_150405")
	return fmt.Sprintf("img_%s_%03d.tiff", s, t.Nanosecond()/1000000)
}

func (c *imageCommand) Run(ctx context.Context, args []string) (string, error) {
	name := genFilename(time.Now())
	file, err := os.Create(name)
	if err != nil {
		return "", err
	}
	defer file.Close()

	ch := make(chan error, 1)
	err = c.client.StartAcq(context.Background(), c.opts...)
	if err != nil {
		return "", err
	}

	if c.useHardware {
		FpgaLoad.Data, err = json.Marshal(c.timing)
		if err != nil {
			return "", err
		}
		err = sendMessages("/run/tb.sock", FpgaDisable, FpgaLoad)
		if err != nil {
			return "", err
		}
	}

	ctx = metadata.AppendToOutgoingContext(ctx, "image-chunksize",
		fmt.Sprintf("%d", 64*1024))

	rdr, err := c.client.Acquire(ctx)
	if err != nil {
		return "", err
	}

	go func() {
		defer close(ch)
		_, err := io.Copy(file, rdr)
		ch <- err
	}()

	if c.useHardware {
		_ = sendMessages("/run/tb.sock", FpgaEnable)
		err = <-ch
		_ = sendMessages("/run/tb.sock", FpgaDisable)
	} else {
		c.client.SendTrigger(context.Background())
		err = <-ch
	}
	c.client.StopAcq(context.Background())

	return name, err
}

type expCommand struct {
	dev    *beamexp.Device
	offset int
}

func (c *expCommand) Help() string {
	return "usage: exp [value]"
}

func (c *expCommand) Synopsis() string {
	return "get/set the beam expander value"
}

func (c *expCommand) Validate(args []string) error {
	if len(args) == 0 {
		return nil
	}

	val, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}
	if val < 0 || val > 3200 {
		return fmt.Errorf("Value out of range")
	}
	c.offset = val

	return nil
}

func (c *expCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *expCommand) getOffset() (string, error) {
	v, err := c.dev.Offset()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d", v), nil
}

func (c *expCommand) Run(ctx context.Context, args []string) (string, error) {
	if len(args) > 0 {
		err := c.dev.SetOffset(c.offset)
		if err != nil {
			return "", err
		}
	}

	return c.getOffset()
}

type laserCommand struct {
	dev  *viron.Device
	conn net.Conn
}

func (c *laserCommand) Help() string {
	return "usage: laser command [args ...]"
}

func (c *laserCommand) Synopsis() string {
	return "send a command to the Viron laser"
}

func (c *laserCommand) Validate(args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("A laser command must be specified")
	}
	return nil
}

func (c *laserCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "wait", Description: "wait until laser is ready to fire"},
		{Text: "temp", Description: "check laser and diode temperature"},
		{Text: "status ?", Description: "check laser status"},
		{Text: "standby", Description: "enter STANDBY mode"},
		{Text: "fire", Description: "fire laser"},
		{Text: "stop", Description: "exit standby mode"},
		{Text: "qsdelay", Description: "get/set Q-switch trigger delay"},
		{Text: "trig", Description: "get/set trigger source"},
	}
	return nil
}

func (c *laserCommand) Run(ctx context.Context, args []string) (string, error) {
	switch arg := strings.ToLower(args[0]); arg {
	case "wait":
		if len(args) > 1 {
			timeout, err := time.ParseDuration(args[1])
			if err != nil {
				return "", err
			}
			c.conn.SetReadDeadline(time.Now().Add(timeout))
		} else {
			c.conn.SetReadDeadline(time.Now().Add(time.Second * 180))
		}
		err := c.dev.WaitForStandby(ctx, nil)
		if err != nil {
			return "", fmt.Errorf("Standby mode not entered: %w", err)
		}
		s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
		s.Prefix = "Waiting for laser warm-up ... "
		s.FinalMSG = "\n"
		ch := make(chan error, 1)
		go func() { ch <- c.dev.WaitForReady(ctx, nil) }()
		s.Start()
		err = <-ch
		s.Stop()
		return "", err
	case "temp":
		c.conn.SetReadDeadline(time.Now().Add(time.Second * 5))
		temp, err := c.dev.Temperature()
		if err != nil {
			return "", err
		}
		return temp.String(), nil
	}

	cmd := strings.ToUpper(args[0])
	// Strip the leading '$' if present
	if cmd[0] == '$' {
		cmd = cmd[1:]
	}

	c.conn.SetReadDeadline(time.Now().Add(time.Second * 5))
	cmdline := viron.NewCommandLine(cmd, args[1:]...)
	resp, err := c.dev.Exec(cmdline)
	return resp.Rval, err
}
