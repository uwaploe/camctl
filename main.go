// Camctl provides an interactive shell to control the inVADER camera
// and intensifier.
package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/beamexp"
	ctl "bitbucket.org/uwaploe/iccdctl"
	"bitbucket.org/uwaploe/imagesvc"
	"bitbucket.org/uwaploe/viron"
	prompt "github.com/c-bata/go-prompt"
	ansi "github.com/k0kubun/go-ansi"
	shellwords "github.com/mattn/go-shellwords"
	"github.com/tarm/serial"
	"google.golang.org/grpc"
)

const USAGE = `Usage: camctl [options]

Interactive shell to control the ICCD image intensifier and camera.
`

var Version = "dev"
var BuildDate = "unknown"

const PROMPT = ">> "

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	intensBaud int    = 115200
	intensAddr int    = 7
	intensDev  string = "/dev/ttyS1"
	expDev     string = "/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907FQ0N-if00-port0"
	expBaud    int    = 9600
	svcAddr    string = "localhost:10130"
	haveEcho   bool   = true
	laserAddr  string = "10.40.7.11:23"
	laserPword string
)

type completer struct {
	cmds map[string]Commander
}

type executor struct {
	cmds map[string]Commander
}

func (c *completer) complete(d prompt.Document) []prompt.Suggest {
	bc := d.TextBeforeCursor()
	if bc == "" {
		return nil
	}

	args := strings.Split(bc, " ")

	var s []prompt.Suggest
	cmd, ok := c.cmds[args[0]]
	if !ok {
		if len(args) == 1 {
			// number of commands + help
			s = make([]prompt.Suggest, len(c.cmds)+1)
			s = append(s,
				prompt.Suggest{Text: "help", Description: "show help message"})
			for name, cmd := range c.cmds {
				s = append(s,
					prompt.Suggest{Text: name, Description: cmd.Synopsis()})
			}
		}
	} else {
		s = cmd.Completion()
	}

	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func (e *executor) execute(l string) bool {
	if l == "quit" || l == "exit" {
		return true
	}

	// Ignore spaces
	if len(strings.TrimSpace(l)) == 0 {
		return false
	}

	parts, err := shellwords.Parse(l)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return false
	}

	if parts[0] == "help" {
		showHelp(os.Stdout, e.cmds)
		return false
	}

	cmd, ok := e.cmds[parts[0]]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command: %q\n", parts[0])
		return false
	}

	var args []string
	if len(parts) != 1 {
		args = parts[1:]
	}

	if err := cmd.Validate(args); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		fmt.Fprint(os.Stderr, cmd.Help())
		return false
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Catch signals
	go func() {
		<-sigs
		cancel()
	}()

	result, err := cmd.Run(ctx, args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
	} else {
		if result != "" {
			fmt.Fprintln(os.Stdout, result)
		}
	}

	return false
}

func showHelp(w io.Writer, cmds map[string]Commander) {
	var maxLen int
	// slice of [name, synopsis]
	text := make([][]string, 0, len(cmds))
	for name, cmd := range cmds {
		text = append(text, []string{name, cmd.Synopsis()})
		if len(name) > maxLen {
			maxLen = len(name)
		}
	}

	text = append(text, []string{"quit", "exit program"})
	if maxLen < 4 {
		maxLen = 4
	}

	var cmdText string
	for name, cmd := range cmds {
		cmdText += fmt.Sprintf("  %-"+strconv.Itoa(maxLen)+"s    %s\n",
			name, cmd.Synopsis())
	}

	fmt.Fprintf(w, "Available commands:\n%s", cmdText)
}

func grpcClient(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

func repl(bus *ctl.Bus, address int, client *imagesvc.Client, beDev *beamexp.Device,
	laserConn net.Conn) {
	dev := ctl.NewIntensifier(bus, address)
	cmds := map[string]Commander{
		"ping":  &pingCommand{bus: bus, target: address},
		"reset": &resetCommand{bus: bus, target: address},
		"gate":  &gateCommand{dev: dev},
		"gain":  &gainCommand{dev: dev},
		"sync":  &syncCommand{dev: dev},
		"trace": &traceCommand{},
		"image": &imageCommand{client: client},
	}

	if beDev != nil {
		cmds["exp"] = &expCommand{dev: beDev}
	}

	if laserConn != nil {
		dev := viron.NewDevice(laserConn)
		if err := dev.StartSession(laserPword); err != nil {
			log.Printf("Cannot log-in to laser: %v", err)
		} else {
			cmds["laser"] = &laserCommand{
				conn: laserConn,
				dev:  dev}
		}
	}

	executor := &executor{cmds: cmds}
	completer := &completer{cmds: cmds}
	defer ansi.CursorShow()

	var input string
	for {
		ansi.CursorShow()
		if runtime.GOOS == "windows" {
			// on windows, use a plain input prompt,
			// as the ANSI codes from the prompt package
			// are not well supported
			fmt.Print(PROMPT)
			scanner := bufio.NewScanner(os.Stdin)
			scanner.Scan()
			input = scanner.Text()
		} else {
			input = prompt.Input(PROMPT, completer.complete)
		}
		ansi.CursorHide()
		if executor.execute(input) {
			break
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, USAGE)
		flag.PrintDefaults()
	}
	flag.StringVar(&svcAddr, "server", lookupEnvOrString("IA_SERVER", svcAddr),
		"host:port for image acquisition server")
	flag.StringVar(&intensDev, "intens-dev",
		lookupEnvOrString("INTENS_DEVICE", intensDev),
		"image intensifier serial device")
	flag.IntVar(&intensBaud, "intens-baud",
		lookupEnvOrInt("INTENS_BAUD", intensBaud),
		"intensifier baud rate")
	flag.IntVar(&intensAddr, "intens-addr",
		lookupEnvOrInt("INTENS_ADDR", intensAddr),
		"intensifier rs-485 bus address")
	flag.BoolVar(&haveEcho, "has-echo", haveEcho,
		"true if the rs-485 interface has an echo")
	flag.StringVar(&expDev, "exp-dev",
		lookupEnvOrString("EXP_DEVICE", expDev),
		"beam expander serial device")
	flag.IntVar(&expBaud, "exp-baud",
		lookupEnvOrInt("EXP_BAUD", expBaud),
		"beam expander baud rate")
	flag.StringVar(&laserAddr, "laser-addr",
		lookupEnvOrString("VIRON_ADDR", laserAddr),
		"Viron laser host:port")
	flag.StringVar(&laserPword, "laser-pword",
		lookupEnvOrString("VIRON_PASSWORD", laserPword),
		"Viron laser password")

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	p1, err := serial.OpenPort(&serial.Config{
		Name:        intensDev,
		Baud:        intensBaud,
		ReadTimeout: time.Second * 5})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", intensDev, err)
	}

	var beDev *beamexp.Device
	if expDev != "" {
		p2, err := serial.OpenPort(&serial.Config{
			Name:        expDev,
			Baud:        expBaud,
			ReadTimeout: time.Second * 5})
		if err != nil {
			log.Fatalf("Cannot open serial device %q: %v", expDev, err)
		}
		beDev = beamexp.New(p2)
	}

	var laserConn net.Conn
	if laserAddr != "" {
		laserConn, err = net.Dial("tcp", laserAddr)
		if err != nil {
			log.Fatalf("Cannot access Viron laser at %q: %v", laserAddr, err)
		}
	}

	conn, err := grpcClient(svcAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := imagesvc.NewClient(conn)
	bus := ctl.NewBus(p1, haveEcho)

	repl(bus, intensAddr, client, beDev, laserConn)
}
