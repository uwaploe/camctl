# Syntronics ICCD Camera Control

This program provides a simple interactive shell to allow control of the
image intensifier on the inVADER project Syntronics ICCD camera.

## Installation

Download the compressed tar archive from the download section of this
respository, unpack and install the binary `camctl` on the inVADER SIC.

## Usage

The program has a number of command-line options, the default values are
usually ok.

``` shellsession
$ camctl --help
Usage: camctl [options]

Interactive shell to control the ICCD image intensifier and camera.
  -exp-baud int
        beam expander baud rate (default 9600)
  -exp-dev string
        beam expander serial device (default "/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907FQ0N-if00-port0")
  -intens-addr int
        intensifier rs-485 bus address (default 7)
  -intens-baud int
        intensifier baud rate (default 115200)
  -intens-dev string
        image intensifier serial device (default "/dev/ttyS1")
  -server string
        host:port for image acquisition server (default "localhost:10130")
  -version
        Show program version information and exit
```

After starting the program, you'll receive a `>>` prompt. Type *help* to get a list of available
commands, *quit* exits the program

``` shellsession
$ camctl
>> help
Available commands:
  trace    enable or disable message tracing
  image    acquire a single image
  exp      get/set the beam expander value
  ping     check that ICCD is responding to commands
  reset    perform a software reset of the ICCD
  gate     get/set intensifier gate source and width
  gain     get/set intensifier gain as a fraction of max value
  sync     get/set sync (trigger) source
>> ping
pong
>> quit
$
```

## Commands

### ping

This command is used to test the communications link, the response will be
`pong`.

``` shellsession
>> ping
pong
>>
```

### gate [[internal|external] width]

With no arguments, show the current gate setting. Otherwise, change the
gate source and width. The width is only required for an internal gate and
is specified as a decimal number with a unit suffix; ns (nanoseconds), us
(microseconds), or ms (milliseconds).

``` shellsession
>> gate internal 2us
>> gate
2µs
>> gate internal 1.2ms
>> gate
1.2ms
>> gate external
>> gate
external
>>
```

### gain [fraction]

With no arguments, show the current gain setting. Otherwise set it as a
fraction of the maximum gain value.

``` shellsession
>> gain
0.5
>> gain 0.3
>> gain
0.3
>>
```

### sync [internal|external]

With no arguments, show the current sync (trigger) source. Otherwise set
it to *internal* or *external*.

``` shellsession
>> sync
internal
>> sync external
>> sync
external
>>
```

### exp [value]

With no argument, show the current Beam Expander divergence value (an
integer between 0 and 3200), otherwise set the value.

### trace [on|off]

Enable or disable intensifier command tracing. This is useful for
debugging serial communication problems.

### reset

This command performs a software reboot of the ICCD and places the system
in its power-up state with all settings restored to their default
values. After running this command, you must run *ping* several times
until you get a valid response.

### image [--exp=EXPOSURE][--gain=dB][--trig=TYPE]

The image command accepts a number of options to set the acquisition
parameters. It prints out the name of the new image file.

| **Option** | **Description**                             | **Example**          |
|------------|---------------------------------------------|----------------------|
| --exp      | image exposure time, value and unit suffix  | 1ms                  |
| --gain     | camera gain value in dB                     | 3.5                  |
| --trig     | camera trigger type                         | hardware or software |

The parameter settings persist across subsequent runs of the *image* command.
