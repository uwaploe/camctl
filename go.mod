module bitbucket.org/uwaploe/camctl

require (
	bitbucket.org/uwaploe/beamexp v0.3.0
	bitbucket.org/uwaploe/iccdctl v0.9.0
	bitbucket.org/uwaploe/imagesvc v0.8.0
	bitbucket.org/uwaploe/tbctl v0.6.1
	bitbucket.org/uwaploe/viron v0.9.0
	github.com/briandowns/spinner v1.9.0
	github.com/c-bata/go-prompt v0.2.3
	github.com/k0kubun/go-ansi v0.0.0-20180517002512-3bf9e2903213
	github.com/mattn/go-shellwords v1.0.5
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/pkg/errors v0.8.1
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
	google.golang.org/grpc v1.27.0
)

replace bitbucket.org/uwaploe/imagesvc => ../pkg/imagesvc

go 1.13
